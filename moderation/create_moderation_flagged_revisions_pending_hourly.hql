--
-- Creates a table for number of pending flagged revisions to be reviewed on an hourly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_moderation_flagged_revisions_pending_hourly.hql \
--         -d table_name=wmf_product.moderation_flagged_revisions_pending_hourly \
--         -d base_directory=/wmf/data/wmf_product/moderation/flagged_revisions_pending_hourly

CREATE TABLE IF NOT EXISTS ${table_name} (
    `wiki_db`                string  COMMENT 'MediaWiki database name',
    `date`                   date    COMMENT 'Day for which the metric is computed over',
    `hour`                   int     COMMENT 'Hour for which the metric is computed over',
    `pending_revision_count` bigint  COMMENT 'Number of pending flagged revisions to be reviewed.',
    `mean_elapsed_secs`      bigint  COMMENT 'Mean number of seconds elapsed since the revisions were flagged for review.'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
