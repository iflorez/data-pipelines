--
-- Creates a table for pageviews to potential vandalism on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_moderation_vandal_pageviews_monthly.hql \
--         -d table_name=wmf_product.moderation_vandal_pageviews_monthly \
--         -d base_directory=/wmf/data/wmf_product/moderation/vandal_pageviews_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name} (
    `month_time`                        timestamp   COMMENT "YYYY-MM-01 00:00:00.000",
    `wiki_db`                           string      COMMENT 'MediaWiki database code',
    `is_user`                           boolean     COMMENT 'Whether pageview was logged by a user or not',
    `platform`                          string      COMMENT 'Platform for which the pageviews belong to: desktop/mobile',         
    `potential_vandal_revision_count`   bigint      COMMENT 'Number of potential vandal revisions (edits)',
    `vandal_pageview_count`             bigint      COMMENT 'Number of pageviews received to potentially vandalized revisions',
    `ns0_article_pageview_count`        bigint      COMMENT 'Number of pageviews received to articles in namespace zero'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;