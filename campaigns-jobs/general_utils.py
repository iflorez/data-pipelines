import requests
from pyspark.sql import functions as F
from datetime import datetime, timedelta

import campaigns_jobs

def sparkDF_convert_dtypes(df, mapping: dict, binary_ts_cols: list = []):
    """
    Converts/ensures data types of a spark dataframe,
    before loading into a table.
    """
    for col, dtype in mapping.items():
        
        if col in binary_ts_cols:
            # from https://stackoverflow.com/a/72110148
            df = df.withColumn(col, F.to_timestamp(F.decode(F.col(col), 'UTF-8'), 'yyyyMMddHHmmss') )
        else:
            df = df.withColumn(col, df[col].cast(dtype))
    return df

def load_query(url):

    response = requests.get(
        url,
        headers={
            "User-Agent": f"campaigns-jobs/{campaigns_jobs.__version__} "
            "(https://gitlab.wikimedia.org/repos/product-analytics/campaigns-jobs)"
            },
        timeout=30
    )
    
    query = response.text
    return query

def update_force_index_usage(query, table_name, force_index_col):
   
    return query.replace(f"{table_name} ", f"(SELECT * FROM {table_name} FORCE INDEX ({force_index_col})) ")

def update_destination_table(
        spark_session, 
        df, 
        destination: str, 
        partition_cols: list = None, 
        mode: str = 'append', 
        tbl_format: str = 'iceberg') -> None:
    
    if partition_cols:
        all_partitions = df.select(*partition_cols).distinct().collect()

        for partition in all_partitions:
            where_clause = " AND ".join([f"{col} = '{partition[col]}'" for col in partition_cols])
            spark_session.sql(
                f"""
                DELETE FROM {destination}
                WHERE {where_clause}
                """
            )
    
    df.write.format(tbl_format).mode(mode).saveAsTable(destination)

def purge_snapshots(spark_session, destination: str, time_partition_col: str = 'snapshot_date', threshold: int = 30) -> None:

    threshold_date = (datetime.today() - timedelta(days=threshold)).strftime('%Y-%m-%d')

    spark_session.sql(
        f"""
        DELETE FROM {destination}
        WHERE {time_partition_col} < '{threshold_date}'
        """
    )

def write_output_file(df, output_path: str, type: str = "tsv", compression: str = "none") -> None:

    allowed_file_types = ['csv', 'tsv', 'parquet']
    assert type in allowed_file_types, \
        f'invalid file type, please use one of the following: {allowed_file_types}'

    if type == "parquet":

        df.coalesce(1).write \
            .mode("overwrite") \
            .parquet(output_path, compression=compression)
    else:

        delimiter = "\t" if type == "tsv" else ","
        
        df.coalesce(1).write \
            .option("header", True) \
            .option("delimiter", delimiter) \
            .mode("overwrite") \
            .csv(output_path, compression=compression)
