import argparse
import sys
import requests
import json

from datetime import datetime, timedelta

from pyspark.sql import SparkSession
from pyspark.sql.types import (
    DateType, 
    StringType,
    BooleanType,
    LongType,
    TimestampType
)

from .mariadb_utils import (
    resolve_mariadb_host_port_for_wikidb,
    resolve_pw_for_analytics_mariadb_replicas,
    df_from_mariadb_replica_adaptive
)

from .general_utils import (
    sparkDF_convert_dtypes,
    load_query,
    update_force_index_usage,
    update_destination_table,
    write_output_file    
)

user_agent = 'https://gitlab.wikimedia.org/repos/product-analytics/campaigns_jobs/-/blob/main/campaigns_jobs'

def parse_args(args):

    parser = argparse.ArgumentParser(
        description="Fetches snapshot of Automoderator's activty from MariaDB-replicas."
    )
    parser.add_argument(
        "--wiki_db",
        help="Database code of the wiki to fetch the snapshot for.",
        required=True
    )
    parser.add_argument(
        "--destination_table",
        help="destination table to store the snapshot in.",
        required=True
    )
    parser.add_argument(
        "--tmp_dir_path",
        help="temporary location to store the output TSV file.",
        required=True
    )
    parser.add_argument(
        '--mariadb_pw_file',
        help="The HDFS file path for the password to connect to the Analytics Replica MariaDB instance.",
        required=True
    )
    return parser.parse_args(args)

def small_wiki_filter_val(wiki):
    
    small_wikis_source = 'https://gitlab.wikimedia.org/repos/modtools/automod-smallwiki-classifier/-/raw/main/small_wikis_filter_automoderator.json'
    
    response = requests.get(
        small_wikis_source,
        headers={
            "User-Agent": user_agent,
        },
        timeout=30
    )
    
    small_wikis = json.loads(response.text)
    
    if wiki in small_wikis['small_wikis']:
        return 1
    else:
        return 0
    
def get_automod_username(spark, wiki):
    
    spark.table("""wmf_product.automoderator_config""")
    
    automod_username = spark.sql(f"""
        SELECT 
            automoderator_user_name
        FROM 
            wmf_product.automoderator_config
        WHERE
            wiki_db = '{wiki}'
        ORDER BY
            snapshot_date DESC
        LIMIT 1
    """).collect()[0]['automoderator_user_name']
    
    return automod_username

query_url = (
    "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/"
    "main/"
    "automoderator/generate_automoderator_monitoring_snapshot_daily.sql"
)

dtypes_map = {
    "snapshot_date": DateType(),
    "wiki_db": StringType(),
    "is_small_wiki": BooleanType(),
    "amr_rev_id": LongType(),
    "amr_rev_dt": TimestampType(),
    "amr_rev_date": DateType(),
    "is_amr_reverted": BooleanType(),
    "amr_rev_parent_id": LongType(),
    "amr_time_to_revert_sec": LongType(),
    "revr_rev_id": LongType(),
    "revr_actor_name": StringType(),
    "revr_rev_dt": TimestampType(),
    "revr_rev_date": DateType(),
    "revr_user_type": StringType(),
    "revr_user_editcount_bucket": StringType(),
    "revam_rev_id": LongType(),
    "revam_rev_dt": TimestampType(),
    "revam_rev_date": DateType(),
    "revam_time_to_revert_sec": LongType(),
    "revam_actor_name": StringType(),
    "revam_user_type": StringType(),
    "is_potential_false_positive": BooleanType(),
}

def main() -> None:

    args = parse_args(sys.argv[1:])

    spark_session = SparkSession.builder.getOrCreate()

    is_small_wiki = small_wiki_filter_val(args.wiki_db)
    automoderator_user_name = get_automod_username(spark_session, args.wiki_db)

    host, port = resolve_mariadb_host_port_for_wikidb(args.wiki_db)

    print(args.wiki_db, is_small_wiki, automoderator_user_name, host, port)

    pw = resolve_pw_for_analytics_mariadb_replicas(args.mariadb_pw_file)

    query = load_query(query_url)

    query = query.format(
        wiki_db=args.wiki_db, 
        is_small_wiki=is_small_wiki, 
        automoderator_user_name=automoderator_user_name,
        # placeholders to fill documentation
        host='tmp', 
        port='tmp'
    )   

    updated_query = update_force_index_usage(query, 'revision', 'rev_timestamp')

    yesterday = datetime.now() - timedelta(days=1)
    upper_bound = yesterday.replace(hour=23, minute=59, second=59).strftime('%Y%m%d%H%M%S')
    
    automod_snapshot = df_from_mariadb_replica_adaptive(
        spark=spark_session, 
        query=updated_query, 
        table='revision', 
        partition_column='rev_timestamp', 
        lower_bound='20240525000000', 
        upper_bound=upper_bound, 
        wiki_db=args.wiki_db,
        host=host, 
        port=port, 
        mariadb_pw=pw,
        partition_column_alias='amr_rev_dt'
    )

    automod_snapshot = sparkDF_convert_dtypes(
        automod_snapshot, 
        dtypes_map,
        binary_ts_cols=['amr_rev_dt', 'revr_rev_dt', 'revam_rev_dt']
    )

    update_destination_table(
        spark_session,
        automod_snapshot,
        args.destination_table,
        ['snapshot_date', 'wiki_db'],
        mode='append',
        tbl_format='iceberg'
    )

    write_output_file(
        automod_snapshot,
        args.tmp_dir_path,
        type='tsv'
    )

if __name__ == "__main__":
    main()
