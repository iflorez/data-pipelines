import dns.resolver
import requests
import subprocess

import math
from datetime import datetime

import campaigns_jobs

def resolve_shard_for_wikidb(wikidb, datacenter='eqiad'):
    # source: https://w.wiki/B2oX (by @xcollazo)
    """
    Given a wikidb (i.e. 'simplewiki'), return the target shard that will contain that database.
    Datacenter can be optionally provided, but the Hadoop cluster so far is only available at eqiad.
    """
    if not hasattr(resolve_shard_for_wikidb, "wikidb_to_shard"):
        resolve_shard_for_wikidb.wikidb_to_shard = {}

        response = requests.get(url=f"https://noc.wikimedia.org/db.php?dc={datacenter}&format=json",
                                headers={
                                    "User-Agent": f"campaigns-jobs/{campaigns_jobs.__version__} "
                                    "(https://gitlab.wikimedia.org/repos/product-analytics/campaigns-jobs)"
                                    },
                                timeout=10)
        data = response.json()
        for shard in data.keys():
            for db in data[shard]['dbs']:
                resolve_shard_for_wikidb.wikidb_to_shard[db] = shard

    if wikidb in resolve_shard_for_wikidb.wikidb_to_shard:
        return resolve_shard_for_wikidb.wikidb_to_shard[wikidb]
    else:
        return "s3"  # As per noc.wikimedia.org, any wiki not hosted on the other sections belongs to s3

def resolve_mariadb_host_port_for_wikidb(wikidb):
    # source: https://w.wiki/B2oX (by @xcollazo)
    """
    Given a wikidb (i.e. 'simplewiki'), return the host and port of the MariaDB instance that will contain a replica
    of that MediaWiki instance's tables.
    """
    shard = resolve_shard_for_wikidb(wikidb)
    answers = dns.resolver.resolve('_' + shard + '-analytics._tcp.eqiad.wmnet', 'SRV')
    host, port = str(answers[0].target), answers[0].port
    return host.rstrip('.'), port


def resolve_pw_for_analytics_mariadb_replicas(password_file):
    # source: https://w.wiki/B2oX (by @xcollazo)
    """
    Shells out to discover the password to be able to access the analytics mariadb replicas.
    The password file permissions must match the current user to succeed.
    """
    pw = subprocess.run(
        f"hdfs dfs -cat {password_file}",
        shell=True,
        stdout=subprocess.PIPE,
        universal_newlines=True
    ).stdout.strip()
    return pw

def df_from_mariadb_replica_standard(spark_session, wiki_db, host, port, query, replicas_pw):

    jdbc = (
        spark_session.read.format("jdbc")
        .option("driver", "com.mysql.cj.jdbc.Driver")
        .option("numPartitions", 1)
        .option("url", f"jdbc:mysql://{host}:{port}/{wiki_db}")
        .option("query", query.format(wiki_db=wiki_db))
        .option("user", "research")
        .option("password", replicas_pw)
        .load()
    )

    return jdbc

def df_from_mariadb_replica_adaptive(
    spark,
    query,
    table,
    partition_column,
    lower_bound,
    upper_bound,
    wiki_db,
    host,
    port,
    mariadb_pw,
    partition_column_alias,
    user='research'
):

    # tweaked version of https://w.wiki/BHfk (by @milimetric)

    opts = {
        'driver': 'com.mysql.cj.jdbc.Driver',
        'url': f'jdbc:mysql://{host}:{port}/{wiki_db}',
        'user': user,
        'password': mariadb_pw
    }

    force_index = f'FORCE INDEX ({partition_column})'

    # get data from information_schema
    opts['dbtable'] = f"""
        (SELECT cardinality FROM information_schema.statistics
         WHERE TABLE_NAME = '{table}' AND TABLE_SCHEMA = '{wiki_db}') t
    """
    count = spark.read.format("mediawiki-jdbc").options(**opts).load().take(1)[0][0]

    # get min/max values for partition column
    opts['dbtable'] = f'(SELECT MIN({partition_column}), MAX({partition_column}) FROM {table} {force_index}) t'
    boundary_df = spark.read.format("mediawiki-jdbc").options(**opts).load()
    table_lower_bound, table_upper_bound = boundary_df.take(1)[0]

    partition_column_type = boundary_df.schema.fields[0].dataType.typeName()

    # partition column filter and estimate ratio
    if partition_column_type == 'binary':
        if len(lower_bound) != 14:
            raise TypeError('Only mw timestamp binary types are supported, like rev_timestamp')

        mw_iso = '%Y%m%d%H%M%S'
        start_timestamp = datetime.strptime(lower_bound, mw_iso)
        end_timestamp = datetime.strptime(upper_bound, mw_iso)
        first_timestamp = datetime.strptime(table_lower_bound.decode('ascii'), mw_iso)
        last_timestamp = datetime.strptime(table_upper_bound.decode('ascii'), mw_iso)

        estimate_ratio = max((end_timestamp - start_timestamp).days, 1) / (last_timestamp - first_timestamp).days

    elif partition_column_type == 'decimal':
        estimate_ratio = max((int(upper_bound) - int(lower_bound)), 1) / (table_upper_bound - table_lower_bound)

    # calculate the number of partitions
    num_partitions = max(1, min(64, round(math.pow(6, math.log(count * estimate_ratio, 10) - 6))))

    opts.update({
        'dbtable': f'({query}) t',
        'partitionColumn': partition_column_alias,
        'lowerBound': lower_bound,
        'upperBound': upper_bound,
        'numPartitions': num_partitions
    })

    return spark.read.format("mediawiki-jdbc").options(**opts).load()