import argparse
import sys
import requests
import re
import ast

from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from pyspark.sql.types import (
    DateType, 
    StringType,
    BooleanType,
)

from .general_utils import sparkDF_convert_dtypes

def parse_args(args):
    
    parser = argparse.ArgumentParser(
        description="Calculates pending flagged revisions to be reviewed."
    )
    
    parser.add_argument(
        "--destination_table",
        help="destination table to update the calculated metrics with.",
        required=True,
    )
    
    parser.add_argument(
        "--output_path",
        help="temporary location to store the output TSV file.",
        required=True,
    )
    
    return parser.parse_args(args)

def fetch_settings():
    
    url = "https://noc.wikimedia.org/conf/InitialiseSettings.php.txt"
    
    response = requests.get(
        url,
        headers={
            "User-Agent": "https://gitlab.wikimedia.org/repos/product-analytics/campaigns-jobs"
        },
        timeout=30
    )
    
    settings = response.text
    return settings

def parse_php_array(array):
    
    array = re.sub(r'//.*|/\*.*?\*/', '', array)
    array = array.replace("=>", ":")
    array = array.replace('false', 'False').replace('true', 'True')
    
    return ast.literal_eval(f"{{{array}}}")
    
def extract_automoderator_config(config) -> dict:
    
    status_pattern = r"'wmgUseAutoModerator'\s*=>\s*\[(.*?)\],"
    usernames_pattern = r"'wgAutoModeratorUsername'\s*=>\s*\[(.*?)\],"
    
    extracted_status = re.search(status_pattern, config, re.DOTALL)
    extracted_usernames = re.search(usernames_pattern, config, re.DOTALL)
    
    status = parse_php_array(extracted_status.group(1))
    usernames = parse_php_array(extracted_usernames.group(1))
    
    am_config = dict(
        automoderator_status=status,
        automoderator_user_name=usernames
    )
    
    return am_config

dtypes_map = {
    'snapshot_date': DateType(),
    'wiki_db': StringType(),
    'automoderator_status': BooleanType(),
    'automoderator_user_name': StringType()
}

def create_config_df(spark_session, config, dtypes_map=dtypes_map):

    dbs = (
        set(config['automoderator_status'].keys())
        .union(config['automoderator_user_name'].keys())
    )

    if 'default' in dbs:
        dbs.remove('default')

    db_info = [
        (
            db,
            config['automoderator_status'].get(db, None),
            config['automoderator_user_name'].get(db, None)
        )
     for db in dbs
    ]

    columns = ['wiki_db', 'automoderator_status', 'automoderator_user_name']

    df = spark_session.createDataFrame(db_info, columns)
    df = df.select(F.current_date().alias('snapshot_date'), *df.columns)

    return sparkDF_convert_dtypes(df, dtypes_map)

def update_automod_config(spark_session, df, destination_table: str) -> None:

    all_partitions = (
        df.select("snapshot_date", "wiki_db")
        .distinct()
        .collect()
    )

    for partition in all_partitions:
        spark_session.sql(
            f"""
            DELETE FROM {destination_table}
            WHERE
                snapshot_date = '{partition['snapshot_date']}'
                AND wiki_db = '{partition['wiki_db']}'
            """
        )
    
    df.write.format("iceberg").mode("append").saveAsTable(destination_table)

def write_tsv(df, output_path) -> None:

    df.coalesce(1).write \
        .option("header", True) \
        .option("delimiter", "\t") \
        .mode("overwrite") \
        .csv(output_path, compression="none")
    
def main() -> None:

    args = parse_args(sys.argv[1:])

    spark_session = SparkSession.builder.getOrCreate()

    init_settings = fetch_settings()
    automod_config = extract_automoderator_config(init_settings)
    config_df = create_config_df(spark_session, automod_config)

    update_automod_config(
        spark_session, config_df, args.destination_table
    )

    write_tsv(
        config_df, args.output_path
    )

    spark_session.stop()

if __name__ == "__main__":
    main()
