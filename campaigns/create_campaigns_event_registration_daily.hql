-- related ticket: https://phabricator.wikimedia.org/T374500
--
-- for day vs date see this table: https://datahub.wikimedia.org/dataset/urn:li:dataset:(urn:li:dataPlatform:hive,wmf_traffic.referrer_daily,PROD)/Schema?is_lineage_mode=false&schemaFilter= which is referenced here https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Iceberg#Changes_to_database_names
--vs https://wikitech.wikimedia.org/wiki/Data_Platform/Data_modeling_guidelines
--
--
--
-- Creates a table for key campaign metrics which are collected on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_campaigns_event_registration_daily.hql \
--         -d table_name=wmf_product.campaigns_event_registration_daily \
--         -d base_directory=/wmf/data/wmf_product/campaigns_product/event_registration_daily

CREATE TABLE IF NOT EXISTS ${table_name} (
    `day_date` date COMMENT 'The day for which the metric is computed over.',
    `n_participants_registered` int COMMENT 'Number of distinct participants registered in a day.',
    `n_new_events` int COMMENT 'Number of new events registered in a day.',
    `n_new_participants_registered` int COMMENT 'Number distinct participants registered in a day who created an account in the last 30 days.',
)


USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
