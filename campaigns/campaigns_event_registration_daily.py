#!/usr/bin/env python
# coding: utf-8

# ## imports

# In[1]:


import datetime
import pandas as pd

import warnings
import wmfdata as wmf
from wmfdata import mariadb, hive, spark
from wmfdata.utils import pct_str, pd_display_all
import datetime


# In[2]:


warnings.filterwarnings('ignore')


# ## daily metrics

# In[3]:


event_daily_basic_query = '''
WITH
   base AS (
        SELECT DISTINCT CAST(DATE(CURRENT_TIMESTAMP) AS DATE) - INTERVAL 1 DAY AS date
    ),
    
    event_info AS (
        SELECT DISTINCT campaign_events.event_id, DATE(event_created_at) AS date
        FROM campaign_events 
        WHERE DATE(event_created_at) = CURDATE() - INTERVAL 1 DAY
    ),
    
    participant_info AS (
        SELECT DISTINCT ce_participants.cep_user_id AS user_id, DATE(cep_registered_at) AS date
        FROM ce_participants
        WHERE DATE(cep_registered_at) = CURDATE() - INTERVAL 1 DAY
    )
    
SELECT
    base.date,
    COUNT(DISTINCT event_id) AS n_new_events, 
    COUNT(DISTINCT user_id) AS n_participants_registered
FROM
    base
    LEFT JOIN event_info ON base.date = event_info.date
    LEFT JOIN participant_info ON base.date = participant_info.date
    '''


# In[4]:


participant_info = '''
SELECT DISTINCT ce_participants.cep_user_id AS user_id
FROM ce_participants
WHERE DATE(cep_registered_at) = CURDATE() - INTERVAL 1 DAY
'''


# In[5]:


data = mariadb.run(event_daily_basic_query, 'wikishared')
participant_names = mariadb.run(participant_info, 'wikishared')

#Now we will incorporate results from one wikishared query into the WHERE clause of a second centralauth query. 
#The results from both queries will then be used in the WHERE clause of a third Hive query. 
id_list = participant_names['user_id'].dropna().unique().tolist()
ids_str = ", ".join(map(str, id_list))  # Map all integers to strings and join them with commas

usernames_query = f'''
WITH newbies AS (SELECT DISTINCT gu_name AS username, gu_registration
                 FROM globaluser  
                 WHERE globaluser.gu_id IN ({ids_str}) AND
                 gu_registration >= DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 31 DAY), '%Y%m%d%H%i%S') AND gu_registration < DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), '%Y%m%d%H%i%S')
                )
SELECT COALESCE(COUNT(username), 0) AS n_new_participants_registered, DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), '%Y-%m-%d') AS date
FROM newbies
'''   
usernames = mariadb.run(usernames_query, 'centralauth')


# In[6]:


usernames['date'] = pd.to_datetime(usernames['date'])
data['date'] = pd.to_datetime(data['date'])

usernames = usernames.set_index('date')
data = data.set_index('date')
#df = data.merge(usernames, left_index=True, right_index=True, how="outer")


# In[7]:


campaigns_event_registration_daily = pd.concat([usernames, data], axis=1)
campaigns_event_registration_daily.reset_index(inplace=True)


# In[8]:


campaigns_event_registration_daily = (
    campaigns_event_registration_daily.astype({
        'date': 'datetime64[ns]',
        'n_new_events': int,
        'n_participants_registered': int,
        'n_new_participants_registered': int,
    })
)


# In[9]:


campaigns_event_registration_daily


# In[ ]:





# ## load data
destination_table = 'wmf_product.campaigns_event_registration_daily'
# In[2]:


spark_session = wmf.spark.get_active_session()

if spark_session == None:
    spark_session = wmf.spark.create_custom_session(app_name='campaigns_registration_daily')
    
spark_session


# In[ ]:


campaigns_event_registration_daily.iteritems = campaigns_event_registration_daily.items
campaigns_event_registration_daily_spark = spark_session.createDataFrame(campaigns_event_registration_daily)
campaigns_event_registration_daily_spark.createOrReplaceTempView('campaigns_event_registration_daily_tmp')


# In[ ]:


spark_session.sql(f"""
INSERT INTO {destination_table}
SELECT * FROM campaigns_event_registration_daily_tmp
""")


# In[ ]:


spark_session.stop()

