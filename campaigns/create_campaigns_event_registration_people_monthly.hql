-- related ticket: https://phabricator.wikimedia.org/T374500

--
-- Creates a table for key campaign metrics which are collected on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_campaigns_event_registration_people_monthly.hql \
--         -d table_name=wmf_product.campaigns_event_registration_people_monthly \
--         -d base_directory=/wmf/data/wmf_product/campaigns_product/event_registration_people_monthly

CREATE TABLE IF NOT EXISTS ${table_name} (
    `month` int COMMENT 'The month for which the metric is computed over.',
    `year` int COMMENT 'year for month over which the metric is computer over',
    `n_organizers` int COMMENT 'Number of distinct organizers organizing new events created in month.',
    `n_participants_registered` int COMMENT 'Number of distinct participants registered in month.',
)

USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
