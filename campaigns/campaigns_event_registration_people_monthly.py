#!/usr/bin/env python
# coding: utf-8

# ## imports

# In[9]:


import datetime
import pandas as pd

import warnings
import wmfdata as wmf
from wmfdata import mariadb, hive, spark
from wmfdata.utils import pct_str, pd_display_all
import datetime


# In[10]:


warnings.filterwarnings('ignore')


# ## monthly metrics

# In[16]:


#DATE(column): Ensures that the time part (if any) is stripped off from your datetime column.
#DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01'): gets first day of the last calendar month; 
#LAST_DAY(CURDATE() - INTERVAL 1 MONTH): gets last day of the previous month.

event_monthly_basic_query = '''
WITH base AS (  
    SELECT
        YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AS year, 
        MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AS month
    ),
    
participant_info AS (
    SELECT DISTINCT ce_participants.cep_user_id AS user_id, 
    YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AS year,
    MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AS month
    FROM ce_participants
    WHERE DATE(cep_registered_at) BETWEEN DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
    ),
    
organizer_info AS (
    SELECT DISTINCT ce_organizers.ceo_user_id AS organizer_id,
    YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AS year,
    MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AS month
    FROM ce_organizers
    WHERE ceo_event_id IN (SELECT campaign_events.event_id 
                           FROM campaign_events 
                           WHERE DATE(event_created_at) BETWEEN DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURDATE() - INTERVAL 1 MONTH))
)
    
SELECT
    base.year, 
    base.month,
    COUNT(DISTINCT user_id) AS n_participants_registered,
    COUNT(DISTINCT organizer_id) AS n_organizers
FROM
    base
    LEFT JOIN participant_info ON base.month = participant_info.month AND base.year = participant_info.year
    LEFT JOIN organizer_info ON base.month = organizer_info.month AND base.year = organizer_info.year
    '''


# In[17]:


campaigns_event_registration_people_monthly = mariadb.run(event_monthly_basic_query, 'wikishared')

data = data.set_index('date')
#df = data.merge(usernames, left_index=True, right_index=True, how="outer")
# In[7]:


campaigns_event_registration_people_monthly = (
    campaigns_event_registration_people_monthly.astype({
        'year': int,
        'month': int,
        'n_participants_registered': int,
        'n_organizers': int,
    })
)


# In[18]:


campaigns_event_registration_people_monthly


# In[ ]:





# ## load data

# In[ ]:


destination_table = 'wmf_product.campaigns_event_registration_people_monthly'


# In[2]:


spark_session = wmf.spark.get_active_session()

if spark_session == None:
    spark_session = wmf.spark.create_custom_session(app_name='campaigns_registration_monthly')
    
spark_session


# In[ ]:


campaigns_event_registration_people_monthly.iteritems = campaigns_event_registration_people_monthly.items
campaigns_event_registration_monthly_spark = spark_session.createDataFrame(campaigns_event_registration_people_monthly)
campaigns_event_registration_monthly_spark.createOrReplaceTempView('campaigns_event_registration_people_monthly_tmp')


# In[ ]:


spark_session.sql(f"""
INSERT INTO {destination_table}
SELECT * FROM campaigns_event_registration_people_monthly_tmp
""")


# In[ ]:


spark_session.stop()

